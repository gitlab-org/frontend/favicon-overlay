# [2.0.0](https://gitlab.com/gitlab-org/frontend/favicon-overlay/compare/v1.0.0...v2.0.0) (2020-12-23)


* refactor!: Break API add set and reset static methods ([45a58f9](https://gitlab.com/gitlab-org/frontend/favicon-overlay/commit/45a58f947efc27531bddf500db55dbf3d95207b1))


### BREAKING CHANGES

* This API wasn't workin for us yo!

# [1.0.0](https://gitlab.com/gitlab-org/frontend/favicon-overlay/compare/v0.1.0...v1.0.0) (2020-12-15)


### Features

* Wire up all the typescripty things ([29e3e1e](https://gitlab.com/gitlab-org/frontend/favicon-overlay/commit/29e3e1ed9e42d701553c2d6953edb75b69d51d98))


### BREAKING CHANGES

* First major release!
