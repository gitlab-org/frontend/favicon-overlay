#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

function kill_server {
  pkill -HUP esbuild
}

cmd="yarn run cyprus:ff"

yarn run start </dev/zero &
yarn run test
status=$?

kill_server || echo "Something went wrong killing the server"

[ $status -eq 0 ] && echo "Test command was successful" || echo "Test failed"
